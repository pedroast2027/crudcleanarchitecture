﻿using AutoMapper;
using NET_CORE_API_1.Models;
using Store.ApplicationCore.DTOs;
using Store.ApplicationCore.Exceptions;
using Store.ApplicationCore.Mappings;
using Store.Infrastructure.Persistence.Repositories;
using System.Security.Cryptography;


//En esta clase estamos validando todos los métodos de ProductRepository.

namespace Store.IntegrationTests.Repositries
{
    public class UserRepositoryTests : IClassFixture<SharedDatabaseFixture>
    {
        private readonly IMapper _mapper;
        private SharedDatabaseFixture Fixture { get; }

        public UserRepositoryTests(SharedDatabaseFixture fixture)
        {
            Fixture = fixture;

            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<GeneralProfile>();
            });

            _mapper = configuration.CreateMapper();
        }

        [Fact]
        public void GetUser_ReturnsAllUsers()
        {
            using (var context = Fixture.CreateContext())
            {
                var repository = new UserRepository(context, _mapper);

                var user = repository.GetUser();

                Assert.Equal(10, user.Count);
            }
        }

        [Fact]
        public void GetUserById_UserDoesntExist_ThrowsNotFoundException()
        {
            using (var context = Fixture.CreateContext())
            {
                var repository = new UserRepository(context, _mapper);
                var userId = 56;

                Assert.Throws<NotFoundException>(() => repository.GetUserById(userId));
            }
        }

        [Fact]
        public void CreateUser_SavesCorrectData()
        {
            using (var transaction = Fixture.Connection.BeginTransaction())
            {
           
                var request = new CreateUserRequest
                {
                    Username = "UserAleatorio",
                    Password = "123"
            };

                using (var context = Fixture.CreateContext(transaction))
                {
                    var repository = new UserRepository(context, _mapper);

                    var user = repository.CreateUser(request);

                }

                using (var context = Fixture.CreateContext(transaction))
                {
                    var urs = new UserRepository(context, _mapper);

                    Assert.NotNull(urs);
                    Assert.Equal(request.Username, request.Username);
                    Assert.Equal(request.Password, request.Password);
                }
            }
        }

        [Fact]
        public void UpdateUser_SavesCorrectData()
        {
            using (var transaction = Fixture.Connection.BeginTransaction())
            {
                var userId = 1;

                var request = new UpdateUserRequest
                {
                    Username = "UserEditado",
                };

                using (var context = Fixture.CreateContext(transaction))
                {
                    var repository = new UserRepository(context, _mapper);

                    repository.UpdateUser(userId, request);
                }

                using (var context = Fixture.CreateContext(transaction))
                {
                    var repository = new UserRepository(context, _mapper);

                    var user = repository.GetUserById(userId);

                    Assert.NotNull(user);
                    Assert.Equal(request.Username, user.Username);
                }
            }
        }

        [Fact]
        public void UpdateUser_UserDoesntExist_ThrowsNotFoundException()
        {
            var userId = 15;

            var request = new UpdateUserRequest
            {
                Username = "UserEditado",
            };

            using (var context = Fixture.CreateContext())
            {
                var repository = new UserRepository(context, _mapper);
                var action = () => repository.UpdateUser(userId, request);

                Assert.Throws<NotFoundException>(action);
            }
        }

        [Fact]
        public void DeleteUserById_EnsuresUserIsDeleted()
        {
            using (var transaction = Fixture.Connection.BeginTransaction())
            {
                var userId = 2;

                using (var context = Fixture.CreateContext(transaction))
                {
                    var repository = new UserRepository(context, _mapper);
                    var users = repository.GetUser();

                    repository.DeleteUserById(userId);
                }

                using (var context = Fixture.CreateContext(transaction))
                {
                    var repository = new UserRepository(context, _mapper);
                    var action = () => repository.GetUserById(userId);

                    Assert.Throws<NotFoundException>(action);
                }
            }
        }

        [Fact]
        public void DeleteUserById_UserDoesntExist_ThrowsNotFoundException()
        {
            var userId = 48;

            using (var context = Fixture.CreateContext())
            {
                var repository = new UserRepository(context, _mapper);
                var action = () => repository.DeleteUserById(userId);

                Assert.Throws<NotFoundException>(action);
            }
        }
    }
}
