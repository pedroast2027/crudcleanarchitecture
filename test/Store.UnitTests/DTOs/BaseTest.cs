﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//El método ValidateModel devuelve todos los errores que tiene el modelo.
//Estas validaciones provienen de las anotaciones que agregamos en el modelo de solicitud en la clase Producto de la carpeta DTO en el proyecto Store.ApplicationCore.


namespace Store.UnitTests.DTOs
{
    public abstract class BaseTest
    {
        public IList<ValidationResult> ValidateModel(object model)
        {
            var validationResults = new List<ValidationResult>();
            var ctx = new ValidationContext(model, null, null);
            Validator.TryValidateObject(model, ctx, validationResults, true);

            return validationResults;
        }
    }
}
