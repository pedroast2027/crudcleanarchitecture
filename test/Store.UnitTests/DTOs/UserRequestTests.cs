﻿using Store.ApplicationCore.DTOs;


//heredamos BaseTest y luego creamos los métodos para probar CreateProductRequest y UpdateProductRequest.

namespace Store.UnitTests.DTOs
{
    public class UserRequestTest : BaseTest
    {
        [Theory]
        [InlineData("Test", "123", 0)]
        public void ValidateModel_CreateUserRequest_ReturnsCorrectNumberOfErrors(string username, string password, int numberExpectedErrors)
        {
            var request = new CreateUserRequest
            {
                Username = username,
                Password = password
            };

            var errorList = ValidateModel(request);

            Assert.Equal(numberExpectedErrors, errorList.Count);
        }

        [Theory]
        [InlineData("Test", "123", 0)]
        public void ValidateModel_UpdateUserRequest_ReturnsCorrectNumberOfErrors(string username, string password, int numberExpectedErrors)
        {
            var request = new UpdateUserRequest
            { 
                Username = username,
                Password = password
            };

            var errorList = ValidateModel(request);

            Assert.Equal(numberExpectedErrors, errorList.Count);
        }
    }
}
