﻿using NET_CORE_API_1.Models;
using System.ComponentModel.DataAnnotations;


namespace Store.ApplicationCore.DTOs
{
    public class CreateDetallesDeMagoRequest
    {
        [Required]
        public string NameMago { get; set; }
        [Required]
        public int HitPoints { get; set; }
        [Required]
        public int Strength { get; set; }
        [Required]
        public int Defense { get; set; }
        [Required]
        public int Intelligence { get; set; }
        [Required]
        public int UserId { get; set; }
    }

    public class UpdateDetallesDeMagoRequest : CreateDetallesDeMagoRequest
    {
        [Required]
        public string NameMago { get; set; }
    }

    public class DetallesDeMagoResponse
    {
        public int Id { get; set; }
        public string NameMago { get; set; }
        public int HitPoints { get; set; }
        public int Strength { get; set; }
        public int Defense { get; set; }
        public int Intelligence { get; set; }
        public int UserId { get; set; }
        public virtual User users { get; set; }
    }
}
