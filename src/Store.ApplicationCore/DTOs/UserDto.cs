﻿using System.ComponentModel.DataAnnotations;

namespace Store.ApplicationCore.DTOs
{
    public class CreateUserRequest
    {
        [Required]
        public string Username { get; set; } = string.Empty;
        [Required]
        [StringLength(10)]
        public string Password { get; set; } = string.Empty;
    }

    public class UpdateUserRequest : CreateUserRequest
    {
        [Required]
        public string Username { get; set; } = string.Empty;
    }

    public class UserResponse
    {
        [Required]
        public string Username { get; set; } = string.Empty;
        [Required]
        [StringLength(10)]
        public byte[] PasswordHash { get; set; }
        [Required]
        [StringLength(10)]
        public byte[] PasswordSalt { get; set; }


    }

    public class UserLogin
    {
        [Required]
        public string Username { get; set; } = string.Empty;
        [Required]
        [StringLength(10)]
        public string Password { get; set; } = string.Empty;


    }
}
