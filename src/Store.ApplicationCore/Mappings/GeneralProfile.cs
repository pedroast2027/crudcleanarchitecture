﻿using AutoMapper;
using NET_CORE_API_1.Models;
using Store.ApplicationCore.DTOs;
using Store.ApplicationCore.Entities;
using Tutorial_NET_CORE_API_1.Models;

namespace Store.ApplicationCore.Mappings
{
    public class GeneralProfile : Profile
    {
        public GeneralProfile()
        {
            //CreateMap<CreateProductRequest, Product>();
            CreateMap<CreateProductRequest, Product>()
                .ForMember(dest =>
                    dest.Id,
                    opt => opt.Ignore()
                )
                .ForMember(dest =>
                    dest.Stock,
                    opt => opt.Ignore()
                )
                .ForMember(dest =>
                    dest.CreatedAt,
                    opt => opt.Ignore()
                )
                .ForMember(dest =>
                    dest.UpdatedAt,
                    opt => opt.Ignore()
                );
            CreateMap<Product, ProductResponse>();




            //CreateMap<CreateUserRequest, User>();
            CreateMap<CreateUserRequest, User>()
                .ForMember(dest =>
                    dest.Id,
                    opt => opt.Ignore()
                )
                 .ForMember(dest =>
                    dest.Username,
                    opt => opt.Ignore()
                )
                .ForMember(dest =>
                    dest.PasswordHash,
                    opt => opt.Ignore()
                )
                .ForMember(dest =>
                    dest.PasswordSalt,
                    opt => opt.Ignore()
                )
                .ForMember(dest =>
                    dest.DetalleMago,
                    opt => opt.Ignore()
                );

            CreateMap<User, UserResponse>();





            CreateMap<CreateDetallesDeMagoRequest, DetallesDeMago>()
                .ForMember(dest =>
                    dest.Id,
                    opt => opt.Ignore()
                )
                 .ForMember(dest =>
                    dest.NameMago,
                    opt => opt.Ignore()
                )
                .ForMember(dest =>
                    dest.HitPoints,
                    opt => opt.Ignore()
                )
                .ForMember(dest =>
                    dest.Strength,
                    opt => opt.Ignore()
                )
                .ForMember(dest =>
                    dest.Defense,
                    opt => opt.Ignore()
                )
                .ForMember(dest =>
                    dest.Intelligence,
                    opt => opt.Ignore()
                )
                .ForMember(dest =>
                    dest.UserId,
                    opt => opt.Ignore()
                );

            CreateMap<DetallesDeMago, DetallesDeMagoResponse>();

        }
    }
}
