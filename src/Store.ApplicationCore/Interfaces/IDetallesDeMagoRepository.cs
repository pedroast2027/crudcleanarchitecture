﻿using Store.ApplicationCore.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace Store.ApplicationCore.Interfaces
{
    public interface IDetallesDeMagoRepository
    {
        List<DetallesDeMagoResponse> GetDetallesDeMago();

        DetallesDeMagoResponse GetDetallesDeMagoById(int IdDetalle);

        void DeleteDetallesDeMagoById(int IdDetalle);

        DetallesDeMagoResponse CreateDetallesDeMago(CreateDetallesDeMagoRequest request);

        DetallesDeMagoResponse UpdateDetallesDeMago(int IdDetalle, UpdateDetallesDeMagoRequest request);
    
    }
}
