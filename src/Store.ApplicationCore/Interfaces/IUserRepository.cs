﻿using Store.ApplicationCore.DTOs;
using System.Collections.Generic;

namespace Store.ApplicationCore.Interfaces
{
    public interface IUserRepository
    {
        List<UserResponse> GetUser();

        UserResponse GetUserById(int userId);

        void DeleteUserById(int userId);

        UserResponse CreateUser(CreateUserRequest request);

        UserResponse UpdateUser(int userId, UpdateUserRequest request);

        bool LoginUser(UserLogin request);
    }
}