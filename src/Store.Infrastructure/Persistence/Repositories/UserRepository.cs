﻿using AutoMapper;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Mvc;
using NET_CORE_API_1.Models;
using Store.ApplicationCore.DTOs;
using Store.ApplicationCore.Exceptions;
using Store.ApplicationCore.Interfaces;
using Store.Infrastructure.Persistence.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;

namespace Store.Infrastructure.Persistence.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly StoreContext _storeContext;

        public static User user = new User();

        private readonly IMapper mapper;
        private SigningCredentials creds;

        public UserRepository(StoreContext storeContext, IMapper mapper)
        {
            _storeContext = storeContext;
            this.mapper = mapper;
        }

        public UserResponse CreateUser(CreateUserRequest request)
        {
            var urs = this.mapper.Map<User>(request);

            try
            {
                CreatePasswordHash(request.Password, out byte[] passwordHash, out byte[] passwordSalt);

                urs.Username = request.Username;
                urs.PasswordHash = passwordHash;
                urs.PasswordSalt = passwordSalt;
                //user2.DetalleMagoId = request.MagoId;


                _storeContext.Users.Add(urs);
                _storeContext.SaveChanges();
                //return urs;
                return this.mapper.Map<UserResponse>(urs);
            }
            catch
            {
                return null;
            }
        }

        
        public void DeleteUserById(int userId)
        {
            var urs = _storeContext.Users.Find(userId);
            if (urs != null)
            {
                _storeContext.Users.Remove(urs);
                _storeContext.SaveChanges();
            }
            else
            {
                throw new NotFoundException();
            }
        }

        public UserResponse GetUserById(int userId)
        {
            var urs = _storeContext.Users.Find(userId);
            if (urs != null)
            {
                return this.mapper.Map<UserResponse>(urs);
            }

            throw new NotFoundException();
        }

        public List<UserResponse> GetUser()
        {
            return _storeContext.Users.Select(p => this.mapper.Map<UserResponse>(p)).ToList();
        }

        public UserResponse UpdateUser(int userId, UpdateUserRequest request)
        {
            var urs = _storeContext.Users.Find(userId);
            if (urs != null)
            { 
                urs.Username = request.Username;

                _storeContext.Users.Update(urs);
                _storeContext.SaveChanges();

                return this.mapper.Map<UserResponse>(urs);
            }

            throw new NotFoundException();
        }

        public bool LoginUser(UserLogin request)
        {

            
            try
            {
                var user_check = _storeContext.Users.Where(x => x.Username == request.Username).First();
                user = user_check;
                if (user.Username != request.Username)
                {
                    return false;
                }
                if (!VerifyPasswordHash(request.Password, user.PasswordHash, user.PasswordSalt))
                {
                    return false;
                }

                //string token = CreateToken(urs);
                return true;
            }
            catch
            {
               return false;
            }
        }

        private void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using (var hmac = new HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }

        private bool VerifyPasswordHash(string password, byte[] passwordHash, byte[] passwordSalt)
        {
            using (var hmac = new HMACSHA512(passwordSalt))
            {
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                return computedHash.SequenceEqual(passwordHash);
            }
        }




        //private string CreateToken(User user)
        //{

        //    List<Claim> claims = new List<Claim>
        //    {
        //        new Claim(ClaimTypes.Name, user.Username)
        //    };

        //    var cred = new SigningCredentials(_storeContext, SecurityAlgorithms.HmacSha512Signature);

        //    var token = new JwtSecurityToken(
        //        claims: claims,
        //        expires: DateTime.Now.AddDays(1),
        //        signingCredentials: creds);

        //    var jwt = new JwtSecurityTokenHandler().WriteToken(token);

        //    return jwt;
        //}


    }
}