﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Store.ApplicationCore.DTOs;
using Store.ApplicationCore.Exceptions;
using Store.ApplicationCore.Interfaces;
using Store.Infrastructure.Persistence.Contexts;
using System.Collections.Generic;
using System.Linq;
using Tutorial_NET_CORE_API_1.Models;

namespace Store.Infrastructure.Persistence.Repositories
{

    public class DetallesDeMagoRepository : IDetallesDeMagoRepository
    {


        private readonly StoreContext _storeContext;
        private readonly IMapper mapper;

        public static DetallesDeMago detalle = new DetallesDeMago();


        public DetallesDeMagoRepository(StoreContext storeContext, IMapper mapper)
        {
            _storeContext = storeContext;
            this.mapper = mapper;
        }

        public DetallesDeMagoResponse CreateDetallesDeMago(CreateDetallesDeMagoRequest request)
        {
            var detall = mapper.Map<DetallesDeMago>(request);

            try
            {
                detall.NameMago = request.NameMago;
                detall.HitPoints = request.HitPoints;
                detall.Strength = request.Strength;
                detall.Defense = request.Defense;
                detall.Intelligence = request.Intelligence;
                detall.UserId = request.UserId;


                _storeContext.DetallesMagos.Add(detall);
            _storeContext.SaveChanges();
            //return urs;
            return mapper.Map<DetallesDeMagoResponse>(detall);
        }
                catch
                {
                    return null;
                }
        }

    public void DeleteDetallesDeMagoById(int IdDetalle)
    {
        var detall = _storeContext.DetallesMagos.Find(IdDetalle);
        if (detall != null)
        {
            _storeContext.DetallesMagos.Remove(detall);
            _storeContext.SaveChanges();
        }
        else
        {
            throw new NotFoundException();
        }
    }

    public List<DetallesDeMagoResponse> GetDetallesDeMago()
        {
            return _storeContext.DetallesMagos.Select(p => mapper.Map<DetallesDeMagoResponse>(p)).ToList();
        }

        public DetallesDeMagoResponse GetDetallesDeMagoById(int IdDetalle)
        {
            var detall = _storeContext.DetallesMagos.FirstOrDefaultAsync(c => c.Id == IdDetalle);
            if (detall == null)
            {
                throw new NotFoundException();
            }
            return mapper.Map<DetallesDeMagoResponse>(detall);
           
        }

        public DetallesDeMagoResponse UpdateDetallesDeMago(int IdDetalle, UpdateDetallesDeMagoRequest request)
        {
            var detall = _storeContext.DetallesMagos.Find(IdDetalle);
            if (detall != null)
            {
                detall.NameMago = request.NameMago;
                detall.HitPoints = request.HitPoints;
                detall.Strength = request.Strength;
                detall.Defense = request.Defense;
                detall.Intelligence = request.Intelligence;

                _storeContext.DetallesMagos.Update(detall);
                _storeContext.SaveChanges();

                return mapper.Map<DetallesDeMagoResponse>(detall);
            }

            throw new NotFoundException();
        }
    }
}