﻿using Microsoft.EntityFrameworkCore;
using NET_CORE_API_1.Models;
using Store.ApplicationCore.Entities;
using Tutorial_NET_CORE_API_1.Models;

namespace Store.Infrastructure.Persistence.Contexts
{
    public class StoreContext : DbContext
    {
        public StoreContext(DbContextOptions<StoreContext> options) : base(options)
        {
        }

        public DbSet<Product> Products { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<DetallesDeMago> DetallesMagos { get; set; }

    }
}