﻿using Microsoft.AspNetCore.Mvc;
using Store.ApplicationCore.DTOs;
using Store.ApplicationCore.Exceptions;
using Store.ApplicationCore.Interfaces;
using System.Collections.Generic;

namespace Store.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DetallesDeMagoController : Controller
    {

        //private SigningCredentials creds;

        private readonly IDetallesDeMagoRepository detallesDeMagoRepository;

        public DetallesDeMagoController(IDetallesDeMagoRepository detallesDeMagoRepository)
        {
            this.detallesDeMagoRepository = detallesDeMagoRepository;
        }

        [HttpGet]
        public ActionResult<List<DetallesDeMagoResponse>> GetDetallesDeMago()
        {

            return Ok(this.detallesDeMagoRepository.GetDetallesDeMago());
        }

        [HttpGet("{id}")]
        public ActionResult GetDetallesDeMagoById(int detallesId)
        {
            try
            {
                var detall = this.detallesDeMagoRepository.GetDetallesDeMagoById(detallesId);
                return Ok(detall);
            }
            catch (NotFoundException)
            {
                return NotFound();
            }
        }


        [HttpPost]
        public ActionResult Create(CreateDetallesDeMagoRequest request)
        {
            try
            {
                var detall = this.detallesDeMagoRepository.CreateDetallesDeMago(request);
                return Ok(detall);
            }
            catch (NotFoundException)
            {
                return NotFound();
            }
        }

        [HttpPut("{id}")]
        public ActionResult Update(int detallesId, UpdateDetallesDeMagoRequest request)
        {
            try
            {
                var detall = detallesDeMagoRepository.UpdateDetallesDeMago(detallesId, request);
                return Ok(detall);
            }
            catch (NotFoundException)
            {
                return NotFound();
            }
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int detallesId)
        {
            try
            {
                this.detallesDeMagoRepository.DeleteDetallesDeMagoById(detallesId);
                return NoContent();
            }
            catch (NotFoundException)
            {
                return NotFound();
            }

        }

    }
}
