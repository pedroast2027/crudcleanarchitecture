﻿using Microsoft.IdentityModel.Tokens;
using Store.ApplicationCore.DTOs;
using Store.ApplicationCore.Interfaces;
using System.Collections.Generic;

namespace NET_CORE_API_1.Controllers
{
    internal class SigningCreadentials
    {
        private SymmetricSecurityKey key;
        private string hmacSha512Signature;
        private IUserRepository userRepository;
        private List<UserResponse> getUser;

        public SigningCreadentials(SymmetricSecurityKey key, string hmacSha512Signature)
        {
            this.key = key;
            this.hmacSha512Signature = hmacSha512Signature;
        }

        public SigningCreadentials(IUserRepository userRepositoryt, string hmacSha512Signature)
        {
            this.userRepository = userRepository;
            this.hmacSha512Signature = hmacSha512Signature;
        }

        public SigningCreadentials(List<UserResponse> getUser, string hmacSha512Signature)
        {
            this.getUser = getUser;
            this.hmacSha512Signature = hmacSha512Signature;
        }
    }
}