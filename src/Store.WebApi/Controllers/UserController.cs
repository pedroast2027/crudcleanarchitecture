﻿using Microsoft.AspNetCore.Mvc;
using Store.ApplicationCore.DTOs;
using Store.ApplicationCore.Exceptions;
using Store.ApplicationCore.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NET_CORE_API_1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : Controller
    {

        //private SigningCredentials creds;

        private readonly IUserRepository userRepository;

        public UserController(IUserRepository userRepository)
        {
            this.userRepository = userRepository;
        }

        [HttpGet]
        public ActionResult<List<UserResponse>> GetUser()
        {

            return Ok(this.userRepository.GetUser());
        }

        [HttpGet("{id}")]
        public ActionResult GetUsersById(int id)
        {
            try
            {
                var usr = this.userRepository.GetUserById(id);
                return Ok(usr);
            }
            catch (NotFoundException)
            {
                return NotFound();
            }
        }


        [HttpPost]
        public ActionResult Create(CreateUserRequest request)
        {
            try
            {
                var usr = this.userRepository.CreateUser(request);
                return Ok(usr);
            }
            catch (NotFoundException)
            {
                return NotFound();
            }
        }

        [HttpPost("Login")]

        public ActionResult Login(UserLogin request)
        {
            try
            {
                var usr = this.userRepository.LoginUser(request);
                if (usr)
                {
                    return Ok(usr);
                }
                else
                {
                    return BadRequest();
                }
                
            }
            catch (NotFoundException)
            {
                return NotFound();
            }
        }

        [HttpPut("{id}")]
        public ActionResult Update(int id, UpdateUserRequest request)
        {
            try
            {
                var usr = this.userRepository.UpdateUser(id, request);
                return Ok(usr);
            }
            catch (NotFoundException)
            {
                return NotFound();
            }
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            try
            {
                this.userRepository.DeleteUserById(id);
                return NoContent();
            }
            catch (NotFoundException)
            {
                return NotFound();
            }

        }

    }
}
