﻿using Microsoft.Extensions.Hosting;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace MVC_Tareas_segundo_plano.BackgroundsTask
{
    public class IntervalTestBackgroundTask : BackgroundService
    {
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested) { 
                for (int i = 5; i < 10; i++)
                {
                    Console.WriteLine($"Hola, soy una tarea en segundo plano: {i}");
                   
                }
            }
            await Task.Delay(2000);
            Console.WriteLine("finis");
        }
    }
}
