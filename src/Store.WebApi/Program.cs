using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MVC_Tareas_segundo_plano.BackgroundsTask;
using Store.ApplicationCore;
using Store.ApplicationCore.Interfaces;
using Store.Infrastructure;
using Store.Infrastructure.Persistence.Repositories;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container
builder.Services.AddApplicationCore();
builder.Services.AddInfrastructure(builder.Configuration);
builder.Services.AddControllers();




builder.Services.AddTransient< IUserRepository, UserRepository >();
builder.Services.AddTransient< IDetallesDeMagoRepository, DetallesDeMagoRepository >();
//builder.Services.AddHostedService<IntervalTestBackgroundTask>();//background tasks

builder.Services.AddSingleton<IHostedService, IntervalTestBackgroundTask>();//background tasks

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseAuthorization();

app.MapControllers();

app.Run();
